#include "image_ppm.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>

void getData(unsigned short* dataShort, unsigned char* dataChar, unsigned int sizeDataChar){
    for(unsigned int i = 0; i < sizeDataChar; i += 2){
        dataShort[i/2] = dataChar[i+1] + (unsigned short)dataChar[i] * 256; 
    }
}

unsigned short getMax(unsigned short* data, unsigned int size){
    unsigned short max = 0;

    for(unsigned int i = 0; i < size; i++){
        if(max < data[i]){
            max = data[i];
        }
    }

    return max;
}

unsigned short getMin(unsigned short* data, unsigned int size){
    unsigned short min = data[0];

    for(unsigned int i = 0; i < size; i++){
        if(min > data[i]){
            min = data[i];
        }
    }

    return min;
}

unsigned short getValue(unsigned short* data, int x, int y, int z, int dimX, int dimY){
    return data[x + y * dimX + z * dimX * dimY];
}

unsigned short getValueByMode(unsigned short* data, int nH, int nW, int mode, int axis, int dimX, int dimY, int dimZ, int i, int j){
    int finalValue = 0; 
    int sizeMax;
    
    if(mode == 3){
        finalValue = data[0];
    }

    sizeMax = axis == 1? dimX : axis == 2? dimY : dimZ;

    for(int a = 0; a < sizeMax; a++){
        int value;
        if(axis == 1){
            value = getValue(data, a, i, j, dimX, dimY);
        }else if(axis == 2){
            value = getValue(data, i, a, j, dimX, dimY);
        }else{
            value = getValue(data, i, j, a, dimX, dimY);
        }

        if(mode == 1){
            finalValue = std::max(finalValue, value);
        }else if(mode == 3){
            finalValue = std::min(finalValue, value);
        }else{
            finalValue += value;
        }
    }

    if(mode == 2){finalValue = finalValue/sizeMax;}

    return (unsigned short)finalValue;
}

void getAndWriteImage(char* fileOut, unsigned short* data, int dimX, int dimY, int dimZ, int axis, int mode){
    OCTET* ImgOut;
    int nW, nH;
    nW = axis == 1 ? dimZ : axis == 2? dimZ : dimY;
    nH = axis == 1 ? dimY : axis == 2? dimX : dimX;
    allocation_tableau(ImgOut, OCTET, nH * nW);
    unsigned short* dataBuffer = new unsigned short[nH * nW];

    for(int i = 0; i < nH; i++){
        for(int j = 0; j < nW; j++){
            dataBuffer[i * nW + j] = getValueByMode(data, nH, nW, mode, axis, dimX, dimY, dimZ, i, j);
        }
    }

    unsigned short max = getMax(dataBuffer, nH*nW);
    unsigned short min = getMin(dataBuffer, nH*nW);

    for(int i = 0; i < nH * nW; i++){
        ImgOut[i] = (unsigned char)(((dataBuffer[i] - min) / double(max-min)) * 255);
    }

    ecrire_image_pgm(fileOut, ImgOut,  nH, nW);
}

int main(int argc, char* argv[]){

    char fileName[250]; char fileOut[250];
    int dimX, dimY, dimZ, visuAxis, visuMode;
    OCTET* ImgOut;

    if(argc != 8){
       printf("Usage: ImgIn ImgOut DimX DimY DimZ visuAxis\n"); 
       exit (1) ; 
    }

    sscanf (argv[1],"%s",fileName);
    sscanf (argv[2],"%s",fileOut);
    sscanf (argv[3],"%d",&dimX);
    sscanf (argv[4],"%d",&dimY);
    sscanf (argv[5],"%d",&dimZ);
    sscanf (argv[6],"%d",&visuAxis);
    sscanf (argv[7],"%d",&visuMode);

    FILE *stream = fopen(fileName, "rb");

    unsigned int dataSize = dimX * dimY * dimZ * 2;
    unsigned char* dataChar = new unsigned char[dataSize];

    if(fread(dataChar, sizeof(unsigned char), dataSize, stream) == 0){
        printf("Erreur dans la lecture du fichier\n"); 
        exit (1) ; 
    }

    unsigned short* dataShort = new unsigned short[dataSize/2];
    getData(dataShort, dataChar, dataSize);

    getAndWriteImage(fileOut, dataShort, dimX, dimY, dimZ, visuAxis, visuMode);

    return 0;
}