#include "image_ppm.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>

void getData(unsigned short* dataShort, unsigned char* dataChar, unsigned int sizeDataChar){
    for(unsigned int i = 0; i < sizeDataChar; i += 2){
        dataShort[i/2] = dataChar[i+1] + (unsigned short)dataChar[i] * 256; 
    }
}

unsigned short getValue(unsigned short* data, int x, int y, int z, int dimX, int dimY){
    return data[x + y * dimX + z * dimX * dimY];
}

unsigned short getMax(unsigned short* data, unsigned int size){
    unsigned short max = 0;

    for(unsigned int i = 0; i < size; i++){
        if(max < data[i]){
            max = data[i];
        }
    }

    return max;
}

unsigned short getMin(unsigned short* data, unsigned int size){
    unsigned short min = data[0];

    for(unsigned int i = 0; i < size; i++){
        if(min > data[i]){
            min = data[i];
        }
    }

    return min;
}

int main(int argc, char* argv[]){

    char fileName[250];
    int dimX, dimY, dimZ;

    if(argc != 5){
       printf("Usage: ImageIn DimX DimY DimZ\n"); 
       exit (1) ; 
    }

    sscanf (argv[1],"%s",fileName) ;
    sscanf (argv[2],"%d",&dimX);
    sscanf (argv[3],"%d",&dimY);
    sscanf (argv[4],"%d",&dimZ);

    FILE *stream = fopen(fileName, "rb");

    unsigned int dataSize = dimX * dimY * dimZ * 2;
    unsigned char* dataChar = new unsigned char[dataSize];

    if(fread(dataChar, sizeof(unsigned char), dataSize, stream) == 0){
        printf("Erreur dans la lecture du fichier\n"); 
        exit (1) ; 
    }

    unsigned short* dataShort = new unsigned short[dataSize/2];
    getData(dataShort, dataChar, dataSize);

    unsigned short minValue = getMin(dataShort, dataSize/2);
    unsigned short maxValue = getMax(dataShort, dataSize/2);

    std::cout<<"Nom du fichier : "<<fileName<<std::endl;
    std::cout<<"Valeur maximum de l'image : "<<maxValue<<std::endl;
    std::cout<<"Valeur minimum de l'image : "<<minValue<<std::endl;

    int Xvalue, Yvalue, Zvalue;
    std::cout<<"Veuillez entrer les coordonées de votre voxel"<<std::endl;
    std::cout<<"X : ";
    std::cin>>Xvalue; std::cout<<std::endl;
    std::cout<<"Y : ";
    std::cin>>Yvalue; std::cout<<std::endl;
    std::cout<<"Z : ";
    std::cin>>Zvalue; std::cout<<std::endl;

    std::cout<<"Valeur du voxel : "<<Xvalue<<"; "<<Yvalue<<"; "<<Zvalue<<" : "
    <<getValue(dataShort, Xvalue, Yvalue, Zvalue, dimX, dimY)<<std::endl;

    return 0;
}