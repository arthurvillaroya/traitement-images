#include <stdio.h>
#include "image_ppm.h"



void erosion(OCTET* ImgParam, OCTET* ImgModif, int nH, int nW){
  for (int i = 1; i < nH-1; i++){
    for (int j = 1; j < nW-1; j++){
      if(ImgParam[i*nW+j] == 255){
        ImgModif[i*nW+j]=255;
        ImgModif[i*nW+j+1]=255;
        ImgModif[i*nW+j-1]=255;
        ImgModif[(i+1)*nW+j-1]=255;
        ImgModif[(i-1)*nW+j-1]=255;
        ImgModif[(i+1)*nW+j+1]=255;
        ImgModif[(i-1)*nW+j+1]=255;
        ImgModif[(i+1)*nW+j]=255;
        ImgModif[(i-1)*nW+j]=255;
      }
    }
  }
}
void dilatation(OCTET* ImgParam, OCTET* ImgModif, int nH, int nW){
  for (int i = 1; i < nH-1; i++){
    for (int j = 1; j < nW-1; j++){
      if(ImgParam[i*nW+j] == 0){
        ImgModif[i*nW+j]=0;
        ImgModif[i*nW+j+1]=0;
        ImgModif[i*nW+j-1]=0;
        ImgModif[(i+1)*nW+j-1]=0;
        ImgModif[(i-1)*nW+j-1]=0;
        ImgModif[(i+1)*nW+j+1]=0;
        ImgModif[(i-1)*nW+j+1]=0;
        ImgModif[(i+1)*nW+j]=0;
        ImgModif[(i-1)*nW+j]=0;
      }
    }
  }
}

void remettreBlanc(OCTET* ImgParam, int nH, int nW){
  for (int i = 0; i < nH; i++){
    for (int j = 0; j < nW; j++){
      ImgParam[i*nW+j]=255;
    }
  }	
}

void remettreNoir(OCTET* ImgParam, int nH, int nW){
  for (int i = 0; i < nH; i++){
    for (int j = 0; j < nW; j++){
      ImgParam[i*nW+j]=0;
    }
  }	
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
    {
      printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
      exit (1) ;
    }
   
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
   
  OCTET *ImgIn, *ImgOut, *ImgOut2;
   
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille);
  allocation_tableau(ImgOut2, OCTET, nTaille);

  for (int i = 0; i < nH; i++){
    for (int j = 0; j < nW; j++){
      ImgOut2[i*nW+j]=255;
    }
  }

  for (int i = 0; i < nH; i++){
    for (int j = 0; j < nW; j++){
      ImgOut[i*nW+j]=255;
    }
  }


  //Dilatation
    dilatation(ImgIn, ImgOut, nH, nW);
    
    dilatation(ImgOut, ImgOut2, nH, nW);
    remettreBlanc(ImgOut, nH, nW);
    
    dilatation(ImgOut2, ImgOut, nH, nW);
    remettreNoir(ImgOut2, nH, nW);

  //Erosion
    erosion(ImgOut, ImgOut2, nH, nW);
    remettreNoir(ImgOut, nH, nW);
    
    erosion(ImgOut2, ImgOut, nH, nW);
    remettreNoir(ImgOut2, nH, nW);
    
    erosion(ImgOut, ImgOut2, nH, nW);
    remettreNoir(ImgOut, nH, nW);
    
    erosion(ImgOut2, ImgOut, nH, nW);
    remettreNoir(ImgOut2, nH, nW);
    
    erosion(ImgOut, ImgOut2, nH, nW);
    remettreNoir(ImgOut, nH, nW);
    
    erosion(ImgOut2, ImgOut, nH, nW);
    remettreBlanc(ImgOut2, nH, nW);

  //Dilatation
    dilatation(ImgOut, ImgOut2, nH, nW);
    remettreBlanc(ImgOut, nH, nW);
    
    dilatation(ImgOut2, ImgOut, nH, nW);
    remettreBlanc(ImgOut2, nH, nW);
    
    dilatation(ImgOut, ImgOut2, nH, nW);

    ecrire_image_pgm(cNomImgEcrite, ImgOut2,  nH, nW);
    free(ImgIn); free(ImgOut2);

   return 1;
}
