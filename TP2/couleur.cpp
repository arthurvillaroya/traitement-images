#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

int plusPetiteValeurVoisine(OCTET* ImgIn, int index, int nW, int nTaille3){
    int plusPetiteVal = 256;

    if(plusPetiteVal > ImgIn[index]){
        plusPetiteVal = ImgIn[index];
    }
    if(plusPetiteVal > ImgIn[index - 3]){
        plusPetiteVal = ImgIn[index-3];
    }
    if(plusPetiteVal > ImgIn[index + 3]){
        plusPetiteVal = ImgIn[index+3];
    }
    if(index - 3*nW > 0){
        if(plusPetiteVal > ImgIn[index - 3*nW]){
           plusPetiteVal = ImgIn[index]; 
        }
    }
    if(index + 3*nW < nTaille3){
        if(plusPetiteVal > ImgIn[index + 3*nW]){
           plusPetiteVal = ImgIn[index]; 
        }
    }

    return plusPetiteVal;
}

int plusGrandeValeurVoisine(OCTET* ImgIn, int index, int nW, int nTaille3){
    int plusGrandeVal = 0;

    if(plusGrandeVal < ImgIn[index]){
        plusGrandeVal = ImgIn[index];
    }
    if(plusGrandeVal < ImgIn[index - 3]){
        plusGrandeVal = ImgIn[index-3];
    }
    if(plusGrandeVal < ImgIn[index + 3]){
        plusGrandeVal = ImgIn[index+3];
    }
    if(index - 3*nW > 0){
        if(plusGrandeVal < ImgIn[index - 3*nW]){
           plusGrandeVal = ImgIn[index]; 
        }
    }
    if(index + 3*nW < nTaille3){
        if(plusGrandeVal < ImgIn[index + 3*nW]){
           plusGrandeVal = ImgIn[index]; 
        }
    }

    return plusGrandeVal;
}


void dilatation(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW){
    int nH3 = nH * 3;
    int nW3 = nW * 3;

    for (int i = 3; i < nH*nW*3 - 4; i+=3){
        ImgOut[i] = plusGrandeValeurVoisine(ImgIn, i, nW, nH*nW*3);
        ImgOut[i + 1] = plusGrandeValeurVoisine(ImgIn, i+1, nW, nH*nW*3);
        ImgOut[i + 2] = plusGrandeValeurVoisine(ImgIn, i+2, nW, nH*nW*3);
    }
}

void erosion(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW){
    int nH3 = nH * 3;
    int nW3 = nW * 3;
    
    for (int i = 3; i < nH*nW*3 - 4; i+=3){
        ImgOut[i] = plusPetiteValeurVoisine(ImgIn, i, nW, nH*nW*3);
        ImgOut[i + 1] = plusPetiteValeurVoisine(ImgIn, i + 1, nW, nH*nW*3);
        ImgOut[i + 2] = plusPetiteValeurVoisine(ImgIn, i + 2, nW, nH*nW*3);
    }
}

void ouverture(OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgOut2, int nH, int nW){
    erosion(ImgOut, ImgOut, nH, nW);
    dilatation(ImgIn, ImgOut2, nH, nW);
}

void fermeture(OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgOut2, int nH, int nW){
    dilatation(ImgIn, ImgOut, nH, nW);
    erosion(ImgOut, ImgOut2, nH, nW);
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, typeExo;
    
    if (argc != 4){
        printf("Usage: ImageIn.ppm ImageOut.ppm QuelExo\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    sscanf (argv[3],"%d",&typeExo);

    OCTET *ImgIn, *ImgOut, *ImgOut2;
    
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    int nTaille3 = nTaille * 3;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(ImgOut2, OCTET, nTaille3);

    switch(typeExo){
        case 1: //Dilatation
            dilatation(ImgIn, ImgOut, nH, nW);
            ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
            break;

        case 2: //Erosion
            erosion(ImgIn, ImgOut, nH, nW);
            ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
            break;
        
        case 3: //Ouverture
            ouverture(ImgIn, ImgOut, ImgOut2, nH, nW);
            ecrire_image_ppm(cNomImgEcrite, ImgOut2,  nH, nW);
            break;

        case 4: //Fermeture
            fermeture(ImgIn, ImgOut, ImgOut2, nH, nW);
            ecrire_image_ppm(cNomImgEcrite, ImgOut2,  nH, nW);
            break;  
    }  

    free(ImgIn);
    return 1;
}
