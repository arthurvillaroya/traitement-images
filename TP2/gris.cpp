#include <stdio.h>
#include "image_ppm.h"

int plusPetiteValeurVoisine(OCTET* ImgIn, int indexX, int indexY, int nH, int nW){
    int plusPetiteVal = 256;
    
    for(int i = -1; i < 2; i++){
        for(int j = -1; j < 2; j++){
           if(plusPetiteVal > ImgIn[(indexX + i) * nW + indexY + j]){
            plusPetiteVal = ImgIn[(indexX + i) * nW + indexY + j];
           }
        }
    }

    return plusPetiteVal;
}

int plusGrandeValeurVoisine(OCTET* ImgIn, int indexX, int indexY, int nH, int nW){
    int plusGrandeVal = 0;
    
    for(int i = -1; i < 2; i++){
        for(int j = -1; j < 2; j++){
           if(plusGrandeVal < ImgIn[(indexX + i) * nW + indexY + j]){
            plusGrandeVal = ImgIn[(indexX + i) * nW + indexY + j];
           }
        }
    }

    return plusGrandeVal;
}


void dilatation(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW){
    for (int i = 0; i < nH; i++){
        for (int j = 0; j < nW; j++){
            ImgOut[i * nW + j] = plusPetiteValeurVoisine(ImgIn, i, j, nH, nW);
        }
    }
}

void erosion(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW){
    for (int i = 0; i < nH; i++){
        for (int j = 0; j < nW; j++){
            ImgOut[i * nW + j] = plusGrandeValeurVoisine(ImgIn, i, j, nH, nW);
        }
    }
}

void ouverture(OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgOut2, int nH, int nW){
    erosion(ImgOut, ImgOut, nH, nW);
    dilatation(ImgIn, ImgOut2, nH, nW);
}

void fermeture(OCTET* ImgIn, OCTET* ImgOut, OCTET* ImgOut2, int nH, int nW){
    dilatation(ImgIn, ImgOut, nH, nW);
    erosion(ImgOut, ImgOut2, nH, nW);
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, typeExo;
    
    if (argc != 4){
        printf("Usage: ImageIn.pgm ImageOut.pgm QuelExo\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    sscanf (argv[3],"%d",&typeExo);

    OCTET *ImgIn, *ImgOut, *ImgOut2;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgOut2, OCTET, nTaille);

    switch(typeExo){
        case 1: //Dilatation
            dilatation(ImgIn, ImgOut, nH, nW);
            ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
            break;

        case 2: //Erosion
            erosion(ImgIn, ImgOut, nH, nW);
            ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
            break;
        
        case 3: //Ouverture
            ouverture(ImgIn, ImgOut, ImgOut2, nH, nW);
            ecrire_image_pgm(cNomImgEcrite, ImgOut2,  nH, nW);
            break;

        case 4: //Fermeture
            fermeture(ImgIn, ImgOut, ImgOut2, nH, nW);
            ecrire_image_pgm(cNomImgEcrite, ImgOut2,  nH, nW);
            break;  
    }  

    free(ImgIn);
    return 1;
}
