#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <cmath>

int moyRegion(OCTET* ImgIn, int nH, int nW, int nTaille4){
    
    double moy = 0;
    
    for (int i = 0; i < nH; i++){
        for (int j = 0; j < nW; j++){
            moy += ImgIn[i*nW+j]/double(nTaille4); 
        }   
    }

    //std::cout<<"Moyenne : "<<moy<<std::endl;
    return moy;
}

double varianceRegion(OCTET* ImgIn, int nH, int nW, int nTaille4){
    
    double variance = 0;
    int moy = moyRegion(ImgIn, nH, nW, nTaille4);
    
    for (int i = 0; i < nH; i++){
        for (int j = 0; j < nW; j++){
            variance += (moy - ImgIn[i*nW+j]) * (moy - ImgIn[i*nW+j])/double(nTaille4); 
        }   
    }

    //std::cout<<"Variance : "<<variance<<std::endl;
    return variance;
}

void recur(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int nTaille, int seuil){
    OCTET *Img1, *Img2, *Img3, *Img4;

    allocation_tableau(Img1, OCTET, nTaille/4);
    allocation_tableau(Img2, OCTET, nTaille/4);
    allocation_tableau(Img3, OCTET, nTaille/4);
    allocation_tableau(Img4, OCTET, nTaille/4);

    OCTET *Img1O, *Img2O, *Img3O, *Img4O;

    allocation_tableau(Img1O, OCTET, nTaille/4);
    allocation_tableau(Img2O, OCTET, nTaille/4);
    allocation_tableau(Img3O, OCTET, nTaille/4);
    allocation_tableau(Img4O, OCTET, nTaille/4);

    //ASSIGNATION 4 ZONES
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            Img1[i*nW/2+j] = ImgIn[i*nW+j]; 
        }   
    }
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            Img2[i*nW/2+j] = ImgIn[i*nW+(j+nW/2)]; 
        }   
    }
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            Img3[i*nW/2+j] = ImgIn[(i+nW/2)*nW+j]; 
        }   
    }
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            Img4[i*nW/2+j] = ImgIn[(i+nW/2)*nW+(j+nW/2)]; 
        }   
    }

    int moy1 = moyRegion(Img1, nH/2, nW/2, nTaille/4);
    int moy2 = moyRegion(Img2, nH/2, nW/2, nTaille/4);
    int moy3 = moyRegion(Img3, nH/2, nW/2, nTaille/4);
    int moy4 = moyRegion(Img4, nH/2, nW/2, nTaille/4);

    double var1 = varianceRegion(Img1, nH/2, nW/2, nTaille/4);
    double var2 = varianceRegion(Img2, nH/2, nW/2, nTaille/4);
    double var3 = varianceRegion(Img3, nH/2, nW/2, nTaille/4);
    double var4 = varianceRegion(Img4, nH/2, nW/2, nTaille/4);

    if(var1 > seuil){recur(Img1, Img1, nH/2, nW/2, nTaille/4, seuil);}
    else{
        for(int i = 0; i < nH/2; i++){
            for(int j = 0; j < nW/2; j++){
                Img1[i*nW/2+j] = moy1; 
            }
        }   
    }

    if(var2 > seuil){recur(Img2, Img2, nH/2, nW/2, nTaille/4, seuil);}
    else{
        for(int i = 0; i < nH/2; i++){
            for(int j = 0; j < nW/2; j++){
                Img2[i*nW/2+j] = moy2; 
            }
        }   
    }

    if(var3 > seuil){recur(Img3, Img3, nH/2, nW/2, nTaille/4, seuil);}
    else{
        for(int i = 0; i < nH/2; i++){
            for(int j = 0; j < nW/2; j++){
                Img3[i*nW/2+j] = moy3; 
            }
        }   
    }

    if(var4 > seuil){recur(Img4, Img4, nH/2, nW/2, nTaille/4, seuil);}
    else{
        for(int i = 0; i < nH/2; i++){
            for(int j = 0; j < nW/2; j++){
                Img4[i*nW/2+j] = moy4; 
            }
        }   
    }

    //IMAGE DE SORTIE
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            ImgOut[i*nW+j] = Img1[i*nW/2+j]; 
        }   
    }
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            ImgOut[i*nW+(j+nW/2)] = Img2[i*nW/2+j]; 
        }   
    }
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            ImgOut[(i+nW/2)*nW+j] = Img3[i*nW/2+j]; 
        }   
    }
    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            ImgOut[(i+nW/2)*nW+(j+nW/2)] = Img4[i*nW/2+j]; 
        }   
    }
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, seuil;
    
    if (argc != 4){
        printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    sscanf (argv[3],"%d",&seuil);
    

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    recur(ImgIn, ImgOut, nH, nW, nTaille, seuil);

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

    return 1;
}


