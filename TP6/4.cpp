#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <cmath>

int moyRegion(OCTET* ImgIn, int Xmin, int Xmax, int Ymin, int Ymax, int nW, int nTaille4){
    
    double moy = 0;
    
    for (int i = Ymin; i < Ymax; i++){
        for (int j = Xmin; j < Xmax; j++){
            moy += ImgIn[i*nW+j]/double(nTaille4); 
        }   
    }

    std::cout<<"Moyenne : "<<moy<<std::endl;
    
    return moy;
}

double ecartRegion(OCTET* ImgIn, int Xmin, int Xmax, int Ymin, int Ymax, int nW, int nTaille4, int moy){
    
    double variance = 0;
    
    for (int i = Ymin; i < Ymax; i++){
        for (int j = Xmin; j < Xmax; j++){
            variance += (moy - ImgIn[i*nW+j]) * (moy - ImgIn[i*nW+j])/double(nTaille4); 
        }   
    }

    std::cout<<"Ecart type : "<<sqrt(variance)<<std::endl;
    return sqrt(variance);
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    if (argc != 3){
        printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    int moy1 = moyRegion(ImgIn, 0, nW/2, 0, nH/2, nW, nTaille/4);
    int moy2 = moyRegion(ImgIn, nW/2, nW, 0, nH/2, nW, nTaille/4);
    int moy3 = moyRegion(ImgIn, 0, nW/2, nH/2, nH, nW, nTaille/4);
    int moy4 = moyRegion(ImgIn, nW/2, nW, nH/2, nH, nW, nTaille/4);

    double ecart1 = ecartRegion(ImgIn, 0, nW/2, 0, nH/2, nW, nTaille/4, moy1);
    double ecart2 = ecartRegion(ImgIn, nW/2, nW, 0, nH/2, nW, nTaille/4, moy2);
    double ecart3 = ecartRegion(ImgIn, 0, nW/2, nH/2, nH, nW, nTaille/4, moy3);
    double ecart4 = ecartRegion(ImgIn, nW/2, nW, nH/2, nH, nW, nTaille/4, moy4);

    for (int i = 0; i < nH/2; i++){
        for (int j = 0; j < nW/2; j++){
            ImgOut[i*nW+j] = moy1; 
        }   
    }

    for (int i = 0; i < nH/2; i++){
        for (int j = nW/2; j < nW; j++){
            ImgOut[i*nW+j] = moy2; 
        }   
    }

    for (int i = nH/2; i < nH; i++){
        for (int j = 0; j < nW/2; j++){
            ImgOut[i*nW+j] = moy3; 
        }   
    }

    for (int i = nH/2; i < nH; i++){
        for (int j = nW/2; j < nW; j++){
            ImgOut[i*nW+j] = moy4; 
        }   
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

    return 1;
}


