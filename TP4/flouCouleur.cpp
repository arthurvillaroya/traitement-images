
#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, nR, nG, nB;
    
    if (argc != 3){
        printf("Usage: ImageIn.ppm ImageOut.ppm Seuil \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;
    OCTET *ImgRouge, *ImgVert, *ImgBleu;
    
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    int nTaille3 = nTaille * 3;
    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(ImgRouge, OCTET, nTaille); 
    allocation_tableau(ImgVert, OCTET, nTaille);  
    allocation_tableau(ImgBleu, OCTET, nTaille);

    planR(ImgRouge, ImgIn, nTaille);  
    planV(ImgVert, ImgIn, nTaille); 
    planB(ImgBleu, ImgIn, nTaille); 

    //Flou Plan Rouge
    OCTET *ImgRouge2;
    allocation_tableau(ImgRouge2, OCTET, nTaille);
    for(int i = 0; i < nTaille; i++){
        ImgRouge2[i] = ImgRouge[i];
    }
    for (int i=1; i < nH-1; i++){
        for (int j=1; j < nW - 1; j++){
        int som=
        ImgRouge[(i-1)*nW+j-1]+ImgRouge[(i-1)*nW+j]+ImgRouge[(i-1)*nW+j+1]
        +ImgRouge[i*nW+j-1]+ImgRouge[i*nW+j]+ImgRouge[i*nW+j+1]
        +ImgRouge[(i+1)*nW+j-1]+ImgRouge[(i+1)*nW+j]+ImgRouge[(i+1)*nW+j+1];
        
        ImgRouge2[i*nW+j]=som/9;
        }
    }

    //Flou Plan Vert
    OCTET *ImgVert2;
    allocation_tableau(ImgVert2, OCTET, nTaille);
    for(int i = 0; i < nTaille; i++){
        ImgVert2[i] = ImgVert[i];
    }
    for (int i=1; i < nH-1; i++){
        for (int j=1; j < nW - 1; j++){
        int som=
        ImgVert[(i-1)*nW+j-1]+ImgVert[(i-1)*nW+j]+ImgVert[(i-1)*nW+j+1]
        +ImgVert[i*nW+j-1]+ImgVert[i*nW+j]+ImgVert[i*nW+j+1]
        +ImgVert[(i+1)*nW+j-1]+ImgVert[(i+1)*nW+j]+ImgVert[(i+1)*nW+j+1];
        
        ImgVert2[i*nW+j]=som/9;
        }
    }

    //Flou PlanBleu
    OCTET *ImgBleu2;
    allocation_tableau(ImgBleu2, OCTET, nTaille);
    for(int i = 0; i < nTaille; i++){
        ImgBleu2[i] = ImgBleu[i];
    }
    for (int i=1; i < nH-1; i++){
        for (int j=1; j < nW - 1; j++){
        int som=
        ImgBleu[(i-1)*nW+j-1]+ImgBleu[(i-1)*nW+j]+ImgBleu[(i-1)*nW+j+1]
        +ImgBleu[i*nW+j-1]+ImgBleu[i*nW+j]+ImgBleu[i*nW+j+1]
        +ImgBleu[(i+1)*nW+j-1]+ImgBleu[(i+1)*nW+j]+ImgBleu[(i+1)*nW+j+1];
        
        ImgBleu2[i*nW+j]=som/9;
        }
    }

    for (int i = 0; i < nTaille3; i += 3){    
        ImgOut[i] = ImgRouge2[i/3];
        ImgOut[i+1] = ImgVert2[i/3];
        ImgOut[i+2] = ImgBleu2[i/3];
    }

    for (int i = 0; i < nTaille3; i += 3){    
        ImgOut[i] = ImgRouge2[i/3];
        ImgOut[i+1] = ImgVert2[i/3];
        ImgOut[i+2] = ImgBleu2[i/3];
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn);
    return 1;
}
