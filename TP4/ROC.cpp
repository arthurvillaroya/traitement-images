#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgLue2[250];
    int nH, nW, nTaille;
   
    if (argc != 3){
        printf("Usage: ImageIn.pgm ImageIn2.pgm\n"); 
        exit (1) ;
    }
      
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgLue2);

    OCTET *ImgIn, *ImgIn2, *ImgOut;
     
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
   
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    allocation_tableau(ImgIn2, OCTET, nTaille);
    lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

    allocation_tableau(ImgOut, OCTET, nTaille);

    double T[256][2];

    double min_youden = 0;
    int seuil = 0;
    for(int S = 0; S < 256; S++){
        for (int i=0; i < nH; i++){
            for (int j=0; j < nW; j++){
                if ( ImgIn[i*nW+j] < S) ImgOut[i*nW+j]=0; else ImgOut[i*nW+j]=255;
            }
        }

        int TP, FP, TN, FN = 0;

        for(int i = 0; i < nTaille; i++){
            if(ImgOut[i] == 0 && ImgIn2[i] == 255){FN++;}
            if(ImgOut[i] == 255 && ImgIn2[i] == 0){FP++;}
            if(ImgOut[i] == 255 && ImgIn2[i] == 255){TP++;}
            if(ImgOut[i] == 0 && ImgIn2[i] == 0){TN++;}
        }

        double sensibilite = double(TP)/double((TP + FN));
        double specificite = double(TN)/double((TN + FP));

        T[S][0] = 1 - specificite;
        T[S][1] = sensibilite;

        std::cout<<"sensibilite : "<<sensibilite<<std::endl;
        std::cout<<"1 - specificite : "<<1 - specificite<<std::endl;
    }

    std::cout<<"Seuil Opti : "<<seuil<<std::endl;
    
    std::ofstream ofs ("ROC.dat", std::ofstream::out);

    for(int i=0; i < 256; i++){
        ofs<<T[i][0]<<"  "<<T[i][1]<<"\n";
    }

    ofs.close();

    free(ImgIn);
    free(ImgIn2);
    return 1;
}
