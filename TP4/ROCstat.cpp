#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgLue2[250];
    int nH, nW, nTaille, S;
   
    if (argc != 4){
        printf("Usage: ImageIn.pgm ImageIn2.pgm ImageOut.pgm Seuil \n"); 
        exit (1) ;
    }
      
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgLue2);
    sscanf (argv[3],"%d",&S);

    OCTET *ImgIn, *ImgIn2, *ImgOut;
     
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
   
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    allocation_tableau(ImgIn2, OCTET, nTaille);
    lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

    allocation_tableau(ImgOut, OCTET, nTaille);
      
    for (int i=0; i < nH; i++){
        for (int j=0; j < nW; j++){
            if ( ImgIn[i*nW+j] < S) ImgOut[i*nW+j]=0; else ImgOut[i*nW+j]=255;
        }
    }

    int TP, FP, TN, FN = 0;

    for(int i = 0; i < nTaille; i++){
        if(ImgOut[i] == 0 && ImgIn2[i] == 255){FN++;}
        if(ImgOut[i] == 255 && ImgIn2[i] == 0){FP++;}
        if(ImgOut[i] == 255 && ImgIn2[i] == 255){TP++;}
        if(ImgOut[i] == 0 && ImgIn2[i] == 0){TN++;}
    }

    std::cout<<"nbr Faux Positif : "<<FP<<std::endl;
    std::cout<<"nbr Faux Négatif : "<<FN<<std::endl;
    std::cout<<"nbr Vrai Positif : "<<TP<<std::endl;
    std::cout<<"nbr Vrai Négatif : "<<TN<<std::endl;

    /*double sensibilite = double(TP)/double((TP + FN));
    double specificite = double(TN)/double((TN + FP));

    std::cout<<"sensibilite : "<<sensibilite<<std::endl;
    std::cout<<"specificite : "<<specificite<<std::endl;*/

    double rappel = double(TP)/(double(TP + FN));
    double precision = double(TP)/(double(TP + FP));
    double F1Score = double(2*TP)/(double(2*TP + FP + FN));

    std::cout<<"rappel : "<<rappel<<std::endl;
    std::cout<<"precision : "<<precision<<std::endl;
    std::cout<<"F1Score : "<<F1Score<<std::endl;

    free(ImgIn);
    free(ImgIn2);
    return 1;
}
