#include <stdio.h>
#include "image_ppm.h"
#include <cmath>

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    if (argc != 3){
        printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    

    OCTET *ImgIn, *ImgOut, *ImgOut2, *ImgOut3;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgOut2, OCTET, nTaille);
    allocation_tableau(ImgOut3, OCTET, nTaille);

    for (int i = 1; i < nH - 1; i++){
        for (int j = 1; j < nW - 1; j++){
            int a,b,c,d,x;
            a = -1 * ImgIn[(i-1)*nW+j];
            b = -1 * ImgIn[i*nW+(j-1)];
            c = -1 * ImgIn[i*nW+(j+1)];
            d = -1 * ImgIn[(i+1)*nW+j];
            x =  4 * ImgIn[i*nW+j];
            ImgOut[i*nW+j] = a+b+c+d+x; 
        }
    }

    for (int i = 0; i < nH - 1; i++){
        for (int j = 0; j < nW - 1; j++){
            int Gx = ImgOut[i*nW+j] - ImgOut[i*nW+(j+1)];
            int Gy = ImgOut[i*nW+j] - ImgOut[(i+1)*nW+j];
            double arctan;
            if(Gx == 0){
                arctan = 0;
            }
            else{
                arctan = atan(Gy/Gx);
            }
            ImgOut2[i*nW+j] = arctan;
        }
    }

    for (int i = 0; i < nH - 1; i++){
        for (int j = 0; j < nW - 1; j++){
            if(ImgOut2[i*nW+j] > M_PI/4){
                if((ImgOut[i*nW+j] <= 0 && ImgOut[(i+1)*nW+j] > 0) || (ImgOut[i*nW+j] > 0 && ImgOut[(i+1)*nW+j] <= 0)){
                    int Gx = ImgIn[i*nW+j] - ImgIn[i*nW+(j+1)];
                    int Gy = ImgIn[i*nW+j] - ImgIn[(i+1)*nW+j];
                    float gradient = sqrt(Gx*Gx + Gy*Gy);
                    ImgOut3[i*nW+j] = gradient;
                }
                else{
                    ImgOut3[i*nW+j] = 0;
                }
            }
            else{
                    if((ImgOut[i*nW+j] <= 0 && ImgOut[i*nW+j+1] > 0) || (ImgOut[i*nW+j] > 0 && ImgOut[i*nW+j+1] <= 0)){
                    int Gx = ImgIn[i*nW+j] - ImgIn[i*nW+(j+1)];
                    int Gy = ImgIn[i*nW+j] - ImgIn[(i+1)*nW+j];
                    float gradient = sqrt(Gx*Gx + Gy*Gy);
                    ImgOut3[i*nW+j] = gradient;
                }
                else{
                    ImgOut3[i*nW+j] = 0;
                }
            }
        }
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut3,  nH, nW);
    free(ImgIn); free(ImgOut);

    return 1;
}
