#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
int cpt = 0;

double distance(double a, double b){
    return abs(a - b);
}

int valeurTransformeInverse(double RepartionImgB[256][2], double RepartionImgR[256][2], int index){
    double probaImgB = RepartionImgB[index][1];
    double distMin = 100000;
    int indexRetenu = 0;

    for(int i = 0; i < 256; i++){
        if(distance(probaImgB, RepartionImgR[i][1]) < distMin){
            distMin = distance(RepartionImgB[index][1], RepartionImgR[i][1]);
            indexRetenu = i;
        }
    }

    if(indexRetenu == 0){
        cpt++;
    }
    return indexRetenu;
}

int main(int argc, char* argv[])
{
    char cNomImgLue1[250], cNomImgLue2[250], cNomImgEcrite[250];
    int nH, nW, nHL, nWL, nTaille, nTailleLena;
    int N = 256;
    
    if (argc != 4){
        printf("Usage: ImageIn1.pgm ImageIn2.pgm ImageOut.pgm \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue1) ;
    sscanf (argv[2],"%s",cNomImgLue2) ;
    sscanf (argv[3],"%s",cNomImgEcrite) ;
    

    OCTET *ImgIn1, *ImgIn2, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
    nTaille = nH * nW;
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nHL, &nWL);
    nTailleLena = nHL * nWL;
    
    allocation_tableau(ImgIn1, OCTET, nTaille);
    lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    allocation_tableau(ImgIn2, OCTET, nTailleLena);
    lire_image_pgm(cNomImgLue2, ImgIn2, nHL * nWL);

    double repartionImgIn[N][2];
    for(int i=0; i<N; i++){
        repartionImgIn[i][0]=i;
        repartionImgIn[i][1]=0;
    }

    for (int i=0; i < nH; i++){
        for (int j=0; j < nW; j++){
            repartionImgIn[ImgIn1[i*nW+j]][1]++;
        }
    }

    for(int i = 1; i < 256; i++){
        repartionImgIn[i][1] += repartionImgIn[i - 1][1];
    }
    for(int i = 0; i < 256; i++){
        repartionImgIn[i][1] /= nTaille;
    }

    double repartionLena[N][2];
    for(int i=0; i<N; i++){
        repartionLena[i][0]=i;
        repartionLena[i][1]=0;
    }

    for (int i=0; i < nHL; i++){
        for (int j=0; j < nWL; j++){
            repartionLena[ImgIn2[i*nW+j]][1]++;
        }
    }

    for(int i = 1; i < 256; i++){
        repartionLena[i][1] += repartionLena[i - 1][1];
    }
    for(int i = 0; i < 256; i++){
        repartionLena[i][1] /= nTailleLena;
    }

    for(int i = 0; i < nTaille; i++){
        ImgOut[i] = valeurTransformeInverse(repartionImgIn, repartionLena, ImgIn1[i]);
        std::cout<<int(ImgIn1[i])<<std::endl;
    }
    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);

    std::cout<<cpt<<std::endl;

    return 1;
}