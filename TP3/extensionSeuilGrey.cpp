#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

#define MIN 0
#define MAX 255
#define POURCENTAGESEUILMIN 0.1;
#define POURCENTAGESEUILMAX 0.9;

int seuilMin(OCTET* ImgIn, int nTaille){
  int T[256];
  double pourcentage = 0.02 * nTaille;

  for (int i = 0; i < 256; ++i){
    T[i] = 0;
  }

  for(int i = 0; i < nTaille; i++){
    T[ImgIn[i]]++;
  }

  int seuilM;
  int cpt = 0;

  int i = 0;

  while(cpt < pourcentage){
    seuilM = i;
    cpt += T[i];
    i++; 
  }

  return seuilM;
}

int seuilMax(OCTET* ImgIn, int nTaille){
  int T[256];
  double pourcentage = 0.98 * nTaille;

  for (int i = 0; i < 256; ++i){
    T[i] = 0;
  }

  for(int i = 0; i < nTaille; i++){
    T[ImgIn[i]]++;
  }

  int seuilM;
  int cpt = 0;

  int i = 0;

  while(cpt < pourcentage){
    seuilM = i;
    cpt += T[i];
    i++; 
  }

  return seuilM;
} 

void remettreASeuil(OCTET* ImgIn, OCTET* ImgS, int nTaille, int seuilMin, int seuilMax){
  for(int i = 0; i < nTaille; i++){
    if(ImgIn[i] < seuilMin){
      ImgS[i] = seuilMin;
    }
    if(ImgIn[i] > seuilMax){
      ImgS[i] = seuilMax;
    }
  }
}

int valMin(OCTET* ImgIn, int nTaille){
  int min = 255;

  for (int i = 0; i < nTaille; i++){
    if(min > ImgIn[i]){
      min = ImgIn[i];
    }
  } 

  return min;
}

int valMax(OCTET* ImgIn, int nTaille){
  int max = 0;

  for (int i = 0; i < nTaille; i++){
    if(max < ImgIn[i]){
      max = ImgIn[i];
    }
  } 

  return max;
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcrite2[250];
  int nH, nW, nTaille, a0, a1;
  double alpha, beta;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm ImgS.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomImgEcrite2);

   OCTET *ImgIn, *ImgOut, *ImgS;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgS, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgS, nH * nW);

   int minSeuil = seuilMin(ImgIn, nTaille);
   int maxSeuil = seuilMax(ImgIn, nTaille);

   remettreASeuil(ImgIn, ImgS, nTaille, minSeuil, maxSeuil);
	
   a0 = valMin(ImgS, nTaille);
   a1 = valMax(ImgS, nTaille);

   alpha = double((MIN*a1 - MAX*a0))/(a1 - a0);
   beta = double((MAX - MIN))/(a1 - a0);

   std::cout<<"SEUIL MIN : "<<minSeuil<<std::endl;
   std::cout<<"SEUIL MAX : "<<maxSeuil<<std::endl;

   std::cout<<"VAL MIN : "<<a0<<std::endl;
   std::cout<<"VAL MAX : "<<a1<<std::endl; 

   std::cout<<"ALPHA : "<<alpha<<std::endl;
   std::cout<<"BETA : "<<beta<<std::endl;

	 for (int i = 0; i < nTaille; i++){
    ImgOut[i] = alpha + beta * ImgS[i];
	 }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite2, ImgS,  nH, nW);
   free(ImgIn);
   return 1;
}
