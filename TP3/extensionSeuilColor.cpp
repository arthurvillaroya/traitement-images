#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

#define MIN 0
#define MAX 255
#define POURCENTAGESEUILMIN 0.1;
#define POURCENTAGESEUILMAX 0.9;

int seuilMin(OCTET* ImgIn, int nTaille3, int color){
    int a = color;

    int T[256];
    double pourcentage = 0.05 * (nTaille3/3);

    for (int i = 0; i < 256; ++i){
        T[i] = 0;
    }

    for(int i = 0; i < nTaille3; i+=3){
        T[ImgIn[i + a]]++;
    }

    int seuilM;
    int cpt = 0;

    int i = 0;

    while(cpt < pourcentage){
        seuilM = i;
        cpt += T[i];
        i++; 
    }

    return seuilM;
}

int seuilMax(OCTET* ImgIn, int nTaille3, int color){
    int a = color;

    int T[256];
    double pourcentage = 0.95 * (nTaille3/3);

    for (int i = 0; i < 256; ++i){
        T[i] = 0;
    }

    for(int i = 0; i < nTaille3; i+=3){
        T[ImgIn[i + a]]++;
    }

    int seuilM;
    int cpt = 0;

    int i = 0;

    while(cpt < pourcentage){
        seuilM = i;
        cpt += T[i];
        i++; 
    }

    return seuilM;
} 

void remettreASeuil(OCTET* ImgIn, OCTET* ImgS, int nTaille3, int seuilMin, int seuilMax, int color){
  for(int i = 0; i < nTaille3; i+=3){
    if(ImgIn[i + color] < seuilMin){
      ImgS[i + color] = seuilMin;
    }
    if(ImgIn[i + color] > seuilMax){
      ImgS[i + color] = seuilMax;
    }
  }
}

int valMin(OCTET* ImgIn, int nTaille3, int color){
  int min = 255;

  for (int i = 0; i < nTaille3; i+=3){
    if(min > ImgIn[i + color]){
      min = ImgIn[i + color];
    }
  } 

  return min;
}

int valMax(OCTET* ImgIn, int nTaille3, int color){
  int max = 0;

  for (int i = 0; i < nTaille3; i+=3){
    if(max < ImgIn[i + color]){
      max = ImgIn[i + color];
    }
  } 

  return max;
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcrite2[250];
    int nH, nW, nTaille, a0R, a1R, a0G, a1G, a0B, a1B;
    double alphaR, betaR, alphaG, betaG, alphaB, betaB;
    
    if (argc != 4) {
        printf("Usage: ImageIn.pgm ImageOut.pgm ImgS.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    sscanf (argv[3],"%s",cNomImgEcrite2);

    OCTET *ImgIn, *ImgOut, *ImgS;
    
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    int nTaille3 = nTaille * 3;
    
    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(ImgS, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgS, nH * nW);

    int minSeuilR = seuilMin(ImgIn, nTaille3, 0);
    int maxSeuilR = seuilMax(ImgIn, nTaille3, 0);
    int minSeuilG = seuilMin(ImgIn, nTaille3, 1);
    int maxSeuilG = seuilMax(ImgIn, nTaille3, 1);
    int minSeuilB = seuilMin(ImgIn, nTaille3, 2);
    int maxSeuilB = seuilMax(ImgIn, nTaille3, 2);

    remettreASeuil(ImgIn, ImgS, nTaille3, minSeuilR, maxSeuilR, 0);
    remettreASeuil(ImgIn, ImgS, nTaille3, minSeuilG, maxSeuilG, 1);
    remettreASeuil(ImgIn, ImgS, nTaille3, minSeuilB, maxSeuilB, 2);
        
    a0R = valMin(ImgS, nTaille3, 0);
    a1R = valMax(ImgS, nTaille3, 0);

    alphaR = double((MIN*a1R - MAX*a0R))/(a1R - a0R);
    betaR = double((MAX - MIN))/(a1R - a0R);

    std::cout<<"SEUIL MIN Rouge : "<<minSeuilR<<std::endl;
    std::cout<<"SEUIL MAX Rouge : "<<maxSeuilR<<std::endl;

    std::cout<<"VAL MIN Rouge : "<<a0R<<std::endl;
    std::cout<<"VAL MAX Rouge : "<<a1R<<std::endl; 

    std::cout<<"ALPHA Rouge : "<<alphaR<<std::endl;
    std::cout<<"BETA Rouge : "<<betaR<<std::endl;

    a0G = valMin(ImgS, nTaille3, 1);
    a1G = valMax(ImgS, nTaille3, 1);

    alphaG = double((MIN*a1G - MAX*a0G))/(a1G - a0G);
    betaG = double((MAX - MIN))/(a1G - a0G);

    std::cout<<"SEUIL MIN Vert : "<<minSeuilR<<std::endl;
    std::cout<<"SEUIL MAX Vert : "<<maxSeuilR<<std::endl;

    std::cout<<"VAL MIN Vert : "<<a0G<<std::endl;
    std::cout<<"VAL MAX Vert : "<<a1G<<std::endl; 

    std::cout<<"ALPHA Vert : "<<alphaG<<std::endl;
    std::cout<<"BETA Vert : "<<betaG<<std::endl;

    a0B = valMin(ImgS, nTaille3, 2);
    a1B = valMax(ImgS, nTaille3, 2);

    alphaB = double((MIN*a1B - MAX*a0B))/(a1B - a0B);
    betaB = double((MAX - MIN))/(a1B - a0B);

    std::cout<<"SEUIL MIN Bleu : "<<minSeuilB<<std::endl;
    std::cout<<"SEUIL MAX Bleu : "<<maxSeuilB<<std::endl;

    std::cout<<"VAL MIN Rouge : "<<a0B<<std::endl;
    std::cout<<"VAL MAX Rouge : "<<a1B<<std::endl; 

    std::cout<<"ALPHA Bleu : "<<alphaB<<std::endl;
    std::cout<<"BETA Bleu : "<<betaB<<std::endl;

    for (int i = 0; i < nTaille3; i+=3){
        ImgOut[i] = alphaR + betaR * ImgS[i];
        ImgOut[i + 1] = alphaG + betaG * ImgS[i + 1];
        ImgOut[i + 2] = alphaB + betaB * ImgS[i + 2];
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
    ecrire_image_ppm(cNomImgEcrite2, ImgS,  nH, nW);
    free(ImgIn);
    return 1;
}
