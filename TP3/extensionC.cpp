#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

#define MIN 0
#define MAX 255

int valMin(OCTET* ImgIn, int nTaille){
  int min = 255;

  for (int i = 0; i < nTaille; i++){
    if(min > ImgIn[i]){
      min = ImgIn[i];
    }
  } 

  return min;
}

int valMax(OCTET* ImgIn, int nTaille){
  int max = 0;

  for (int i = 0; i < nTaille; i++){
    if(max < ImgIn[i]){
      max = ImgIn[i];
    }
  } 

  return max;
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, a0R, a1R, a0G, a1G, a0B, a1B;
  double alphaR, betaR, alphaG, betaG, alphaB, betaB;
  
  if (argc != 3) {
    printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
    exit (1) ;
  }
   
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);

  OCTET *ImgIn, *ImgOut, *ImgR, *ImgG, *ImgB;
   
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  
  allocation_tableau(ImgIn, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
  allocation_tableau(ImgOut, OCTET, nTaille3);

  allocation_tableau(ImgR, OCTET, nTaille);
  allocation_tableau(ImgG, OCTET, nTaille);
  allocation_tableau(ImgB, OCTET, nTaille);

  for(int i = 0; i < nTaille3; i += 3){
    ImgR[i/3] = ImgIn[i];
    ImgG[i/3] = ImgIn[i+1];
    ImgB[i/3] = ImgIn[i+2];
  }
	
  a0R = valMin(ImgR, nTaille);
  a1R = valMax(ImgR, nTaille);

  a0G = valMin(ImgG, nTaille);
  a1G = valMax(ImgG, nTaille);

  a0B = valMin(ImgB, nTaille);
  a1B = valMax(ImgB, nTaille);

  alphaR = double((MIN*a1R - MAX*a0R))/(a1R - a0R);
  betaR = double((MAX - MIN))/(a1R - a0R);

  alphaG = double((MIN*a1G - MAX*a0G))/(a1G - a0G);
  betaG = double((MAX - MIN))/(a1G - a0G);

  alphaB = double((MIN*a1R - MAX*a0B))/(a1B - a0B);
  betaB = double((MAX - MIN))/(a1B - a0B);

  std::cout<<"VAL MIN Rouge : "<<a0R<<std::endl;
  std::cout<<"VAL MAX Rouge : "<<a1R<<std::endl; 

  std::cout<<"ALPHA Rouge : "<<alphaR<<std::endl;
  std::cout<<"BETA Rouge : "<<betaR<<std::endl;

  std::cout<<"VAL MIN Vert : "<<a0G<<std::endl;
  std::cout<<"VAL MAX Vert: "<<a1G<<std::endl; 

  std::cout<<"ALPHA Vert : "<<alphaG<<std::endl;
  std::cout<<"BETA Vert : "<<betaG<<std::endl;

  std::cout<<"VAL MIN Bleu : "<<a0B<<std::endl;
  std::cout<<"VAL MAX Bleu : "<<a1B<<std::endl; 

  std::cout<<"ALPHA Bleu : "<<alphaB<<std::endl;
  std::cout<<"BETA Bleu : "<<betaB<<std::endl;

	for (int i = 0; i < nTaille3; i+=3){
   ImgOut[i] = alphaR + betaR * ImgR[i/3];
   ImgOut[i + 1] = alphaG + betaG * ImgG[i/3];
   ImgOut[i + 2] = alphaB + betaB * ImgB[i/3];
	}

  ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn);
  return 1;
}
