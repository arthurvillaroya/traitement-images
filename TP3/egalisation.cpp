#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    int N = 256;
    
    if (argc != 3) 
        {
        printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
        exit (1) ;
        }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite) ;
    

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    double T[N][2];
    for(int i=0; i<N; i++){
        T[i][0]=i;
        T[i][1]=0;
    }

    for (int i=0; i < nH; i++){
        for (int j=0; j < nW; j++){
        T[ImgIn[i*nW+j]][1]++;
        }
    }

    for(int i = 1; i < 256; i++){
        T[i][1] += T[i - 1][1];
    }
    for(int i = 0; i < 256; i++){
        T[i][1] /= nTaille;
    }


    for(int i = 0; i < nTaille; i++){
        ImgOut[i] = T[ImgIn[i]][1] * 255;
    }
    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);


    return 1;
}