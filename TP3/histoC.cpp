#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  int N = 256;
  
  if (argc != 2) 
     {
       printf("Usage: ImageIn.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;


   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
  
   int T[N][4];
   for(int i=0; i<N; i++){
     T[i][0]=i;
     T[i][1]=0;
     T[i][2]=0;
     T[i][3]=0;
   }

    for (int i=0; i < nTaille3; i+=3)
     {
       T[ImgIn[i]][1]++;
       T[ImgIn[i+1]][2]++;
       T[ImgIn[i+2]][3]++;
     }

  std::ofstream ofs ("histoC.dat", std::ofstream::out);

  for(int i=0; i < N; i++)
  {
    ofs<<T[i][0]<<"  "<<T[i][1]<<"  "<<T[i][2]<<"  "<<T[i][3]<<"\n";
  }

  ofs.close();


  return 1;
}