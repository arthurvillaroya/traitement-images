#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

#define MIN 0
#define MAX 255

int valMin(OCTET* ImgIn, int nTaille){
  int min = 255;

  for (int i = 0; i < nTaille; i++){
    if(min > ImgIn[i]){
      min = ImgIn[i];
    }
  } 

  return min;
}

int valMax(OCTET* ImgIn, int nTaille){
  int max = 0;

  for (int i = 0; i < nTaille; i++){
    if(max < ImgIn[i]){
      max = ImgIn[i];
    }
  } 

  return max;
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, a0, a1;
  double alpha, beta;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   a0 = valMin(ImgIn, nTaille);
   a1 = valMax(ImgIn, nTaille);

   alpha = double((MIN*a1 - MAX*a0))/(a1 - a0);
   beta = double((MAX - MIN))/(a1 - a0);

   std::cout<<"VAL MIN : "<<a0<<std::endl;
   std::cout<<"VAL MAX : "<<a1<<std::endl; 

   std::cout<<"ALPHA : "<<alpha<<std::endl;
   std::cout<<"BETA : "<<beta<<std::endl;

	 for (int i = 0; i < nTaille; i++){
    ImgOut[i] = alpha + beta * ImgIn[i];
	 }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}
